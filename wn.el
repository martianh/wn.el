;;; wn.el --- Interface for wordnik.com -*- lexical-binding:t -*-
;;
;; Author: Marty Hiatt <martianhiatus AT riseup.net>
;; Copyright (C) 2023 Marty Hiatt <martianhiatus AT riseup.net>
;;
;; Package-Requires: ((emacs "28.1") (aio "1.0"))
;; Keywords: convenience, translate, wp, dictionary
;; URL: https://codeberg.org/martianh/wn.el
;; Version: 0.2
;; Prefix: wn
;; Separator: -
;;
;;; License:
;;
;; This file is NOT part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Functions to query the Wordnik API. See https://developer.wordnik.com/docs.

;;; Code:
(require 'xml)
(require 'json)
(require 'url-http)
(require 'url)
(require 'dom)
(require 'transient)
(require 'aio)
(require 'eytm nil :no-error)

(declare-function etym-query "etym")

(defvar url-http-end-of-headers)

(defcustom wn-api-key nil
  "Your API key.
Apply for one at <URL:http://www.wordnik.com/signup/>."
  :type 'string
  :group 'wn)

(defcustom wn-include-related "false"
  "Whether to include related terms in results."
  :type 'string
  :group 'wn)

(defcustom wn-use-canonical "true"
  "Whether to include base terms in results.
E.g. \"cats\" will also return results for \"cat\"."
  :type 'string
  :group 'wn)

(defcustom wn-examples-limit "20"
  "Limit for examples results."
  :type 'string
  :group 'wn)

(defvar wn-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "?") #'wn-dispatch)
    (define-key map (kbd "d") #'wn-define-go)
    (define-key map (kbd "x") #'wn-examples-go)
    (define-key map (kbd "r") #'wn-related-go)
    (define-key map (kbd "e") #'wn-etymology-go)
    (when (require 'eytm nil :no-error)
      (define-key map (kbd "E") #'wn-etym-query))
    (define-key map (kbd "p") #'wn-phrases-go)
    (define-key map (kbd "b") #'wn-definition-browse)
    map)
  "Keymap for wn mode.")

(transient-define-prefix wn-dispatch ()
  "Wordnik results commands"
  ["Wordnik results commands"
   [("d" "show definitions" wn-define-go)
    ("x" "show examples" wn-examples-go)
    ("r" "show related" wn-related-go)
    ("e" "show etymologies" wn-etymology-go)
    ("p" "show phrases (bi-gram)" wn-phrases-go)
    ("b" "browse definition on wordnik" wn-definition-browse)
    ("E" "show `etym.el' etymology" wn-etym-query)]])

(defvar wn-api-url "https://api.wordnik.com/v4/")

(defvar-local wn-query nil
  "Details about the current query.")

(defvar-local wn-query-url nil
  "The Wordnik URL for the current query.")

;; all requests are GET
(aio-defun wn-request (query type endpoint &optional params)
  "Wordnik API request for QUERY.
TYPE is a string, either \"word\" or \"words\".
ENDPOINT is a string, the endpoint to hit.
PARAMS is any data parameters to send."
  (let* ((params-str (url-build-query-string params))
         ;; (url-request-data params) ; doesn't send the api key
         (url (format "%s%s.json/%s/%s?%s"
                      wn-api-url type query endpoint params-str)))
    (aio-await (aio-url-retrieve url))))

(aio-defun wn-parse-response (query type endpoint &optional params)
  "Make a request to wordnik and parse the JSON response.
QUERY is the word to look up.
TYPE is \"word\" or \"words\".
ENDPOINT is the endpoint to hit, a string.
PARAMS is any data parameters to send."
  (let* ((response (aio-await (wn-request query type endpoint params)))
         (r-buf (cdr response))
         (json-object-type 'alist)
         (json-array-type 'list)
         (json-key-type 'string))
    (with-current-buffer r-buf
      ;; (switch-to-buffer r-buf)
      (let ((status (url-http-parse-response)))
        (if (= 200 status)
            (progn
              (goto-char (point-min))
              (goto-char url-http-end-of-headers)
              (json-read))
          ;;TODO: basic error handling
          )))))

(defun wn-buffer-print (query print-fn &rest args)
  "Create a new wordnik buffer for QUERY and insert contents.
PRINT-FN is the function used to insert contents.
It is called on ARGS."
  (let ((inhibit-read-only t)
        (buf (get-buffer-create
              (format "*wordnik-%s*" query))))
    (with-current-buffer buf
      (erase-buffer)
      (wn-mode)
      (funcall print-fn (car args)) ; why CAR?
      (goto-char (point-min))
      (when (null wn-query)
        (setq wn-query query)))
    (unless (eq buf (current-buffer))
      (switch-to-buffer-other-window buf))))

(defun wn-parse-xml (xml)
  "Parse XML string."
  (with-temp-buffer
    (insert xml)
    (libxml-parse-html-region (point-min) (point-max))))

(aio-defun wn-etymology (query)
  "Display the etymology response for QUERY."
  (let* ((params `(("api_key" ,wn-api-key)))
         ;; ("useCanonical" ,wn-use-canonical)))
         (json (aio-await (wn-parse-response query "word" "etymologies" params)))
         (xml (wn-parse-xml (car json)))
         (etym (dom-by-tag xml 'ety))
         (str (mapconcat #'identity (dom-strings etym) "|")))
    (wn-buffer-print query 'insert str)))

;;;###autoload
(defun wn-etymology-query ()
  "Prompt for a term and query the wordnik API for etymologies."
  (interactive)
  (let ((query (read-string "Wordnik etymology: ")))
    (wn-etymology query)))

(defun wn-alist-string (str-key alist)
  "Call `alist-get' with `equal' test function.
STR-KEY is the key to use, ALIST is what it says."
  (alist-get str-key alist nil nil 'equal))

(defun wn-print-labels (labels)
  "Print domain or similar LABELS."
  (propertize
   (concat "["
           (upcase (mapconcat
                    (lambda (lab)
                      (wn-alist-string "text" lab))
                    labels
                    "/"))
           "]")
   'face 'font-lock-comment-face))

(defun wn-print-definition-examples (egs)
  "Print a definintion's examples EGS."
  (propertize (mapconcat
               (lambda (eg)
                 (wn-alist-string "text" eg))
               egs
               "\n")
              'face '(:inherit font-lock-comment-face :height 0.8)))

(defun wn-print-definition (def)
  "Print single definition DEF."
  (let ((pos (wn-alist-string"partOfSpeech" def))
        (text (wn-alist-string "text" def))
        (dict (wn-alist-string "sourceDictionary" def))
        (egs (wn-alist-string "exampleUses" def))
        (labels (wn-alist-string "labels" def))
        (wnurl (wn-alist-string "wordnikUrl" def))
        (related (wn-alist-string "relatedWords" def)))
    (when text ; only print if we have a def
      (insert (concat (when pos
                        (propertize pos
                                    'face 'font-lock-comment-face))
                      (when labels
                        (concat " "
                                (wn-print-labels labels)))
                      "\n"
                      text
                      "\n"
                      (when egs
                        (concat (wn-print-definition-examples egs)
                                "\n"))
                      dict
                      "\n\n"))
      (when (null wn-query-url)
        (setq wn-query-url wnurl)))))

(defun wn-print-definitions (json)
  "Print JSON, a set of definitions."
  (mapc #'wn-print-definition json))

(aio-defun wn-definition (query)
  "Display the definition response for QUERY."
  (let* ((params `(("api_key" ,wn-api-key)
                   ("useCanonical" ,wn-use-canonical)
                   ("includeRelated" ,wn-include-related)))
         (json (aio-await (wn-parse-response query "word" "definitions" params))))
    (wn-buffer-print query 'wn-print-definitions json)))

;;;###autoload
(defun wn-definition-query ()
  "Prompt for a term and query the wordnik API for definitions."
  (interactive)
  (let ((query (read-string "Wordnik definition: ")))
    (wn-definition query)))

(defun wn-print-related (rel)
  "Insert a related words result REL."
  (let ((type (wn-alist-string "relationshipType" rel))
        (words (wn-alist-string "words" rel)))
    (when words
      (insert (concat type
                      "\n"
                      (mapconcat #'identity words ", ")
                      "\n\n")))))

(defun wn-print-relateds (json)
  "Insert related words results in JSON."
  (mapc #'wn-print-related json))

(aio-defun wn-related (query)
  "Display the related words response for QUERY."
  (let* ((params `(("api_key" ,wn-api-key)
                   ("useCanonical" ,wn-use-canonical)
                   ;; ("relationshipTypes" ,wn-include-related))
                   ))
         (json (aio-await (wn-parse-response query "word" "relatedWords" params))))
    (wn-buffer-print query 'wn-print-relateds json)))

;;;###autoload
(defun wn-related-query ()
  "Prompt for a term and query the wordnik API for related words."
  (interactive)
  (let ((query (read-string "Wordnik related words: ")))
    (wn-related query)))

(defun wn-print-example (ex)
  "Insert an example result EX."
  (let ((title (wn-alist-string "title" ex))
        (text (wn-alist-string "text" ex))
        (url (wn-alist-string "url" ex)))
    (when text
      (insert text
              "\n"
              (if title title "")
              "\n"
              (if url url "")
              "\n\n"))))

(defun wn-print-examples (json)
  "Insert example words results in JSON."
  (mapc #'wn-print-example
        (wn-alist-string "examples" json)))

(aio-defun wn-examples (query)
  "Display the example words response for QUERY."
  (let* ((params `(("api_key" ,wn-api-key)
                   ("useCanonical" ,wn-use-canonical)
                   ("limit" ,wn-examples-limit)))
         (json (aio-await (wn-parse-response query "word" "examples" params))))
    (wn-buffer-print query 'wn-print-examples json)))

;;;###autoload
(defun wn-examples-query ()
  "Prompt for a term and query the wordnik API for example words."
  (interactive)
  (let ((query (read-string "Wordnik example words: ")))
    (wn-examples query)))

(defun wn-print-phrase (phr)
  "Print a phrase (bi-gram) result PHR."
  (let ((g1 (wn-alist-string "gram1" phr))
        (g2 (wn-alist-string "gram2" phr)))
    (when g1
      (insert g1
              " > "
              g2
              "\n\n"))))

(defun wn-print-phrases (json)
  "Insert phrases results in JSON."
  (mapc #'wn-print-phrase json))

(aio-defun wn-phrases (query)
  "Display the phrases (bi-gram) response for QUERY."
  (let* ((params `(("api_key" ,wn-api-key)
                   ("useCanonical" ,wn-use-canonical)
                   ("limit" 30)))
         (json (aio-await (wn-parse-response query "word" "phrases" params))))
    (wn-buffer-print query 'wn-print-phrases json)))

;;;###autoload
(defun wn-phrases-query ()
  "Prompt for a term and query the wordnik API for bi-gram phrases."
  (interactive)
  (let ((query (read-string "Wordnik phrases: ")))
    (wn-phrases query)))

(defun wn-etymology-go ()
  "Jump to etymologies view of the current query."
  (interactive)
  (wn-etymology wn-query))

(defun wn-define-go ()
  "Jump to definitions view of the current query."
  (interactive)
  (wn-definition wn-query))

(defun wn-definition-browse ()
  "View the current definition in the browser."
  (interactive)
  (if wn-query-url
      (browse-url wn-query-url)
    (message "No query URL found.")))

(defun wn-related-go ()
  "Jump to related words view of the current query."
  (interactive)
  (wn-related wn-query))

(defun wn-examples-go ()
  "Jump to examples view of the current query."
  (interactive)
  (wn-examples wn-query))

(defun wn-phrases-go ()
  "Jump to phrases view of the current query."
  (interactive)
  (wn-phrases wn-query))

(define-derived-mode wn-mode special-mode "wn"
  :group 'wn
  (read-only-mode 1))

(defun wn-etym-query ()
  "Search `etym.el' (etymologyonline.com) with the current query."
  (interactive)
  (etym-query wn-query))

(provide 'wn)
;;; wn.el ends here
